package application;

import akka.actor.*;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import com.ib.client.Contract;
import prc.ConfigReader;
import prc.ib.Connection;
import prc.ib.SecType;
import prc.messaging.akka.EventTopic;
import prc.messaging.akka.LookupBusImpl;
import prc.messaging.akka.Prefix;
import prc.messaging.zeromq.PublisherActor;
import prc.messaging.zeromq.ReqReplyActor;
import prc.trading.ContractLookUp;
import prc.trading.conf.ContractConfig;
import prc.trading.strategy.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * User: axs
 * Date: 12/22/2014
 * Time: 2:51 PM
 */
public class MonarchMain {
    private static LoggingAdapter log;

    public static void printHeader() {
        String header = "\n\n***********************************************************************" +
                "\n*  Monarch SYSTEM" +
                "\n*  Version: docs/VERSION" +
                "\n*  Website: http://audubontechnology.appspot.com/ " +
                "\n***********************************************************************\n\n";
        log.info(header);
        log.info("\n\nPress enter to proceed");

        waitForKey();
    }

    public static void waitForKey() {
        try {
            new BufferedReader(new InputStreamReader(System.in)).readLine();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static Map<Integer, Contract> setupContracts() {
        ContractLookUp contractLookUp = ContractLookUp.getInstance();
        ContractConfig[] contracts = ConfigReader.readContractConfigFile();
        int id = 1;
        Map<Integer, Contract> contractMap = new HashMap<>();
        for (ContractConfig i : contracts) contractMap.put(id++, i.toContract());
        contractLookUp.setContractMap(contractMap);
        return contractMap;
    }

    public static void main(String args[]) {
        final LookupBusImpl lookupBus = new LookupBusImpl();

        final ActorSystem system = ActorSystem.create("Monarch");
        log = Logging.getLogger(system, "monarch");
        printHeader();

        Map<Integer, Contract> contractMap = setupContracts();

        final ActorRef deadActor = system.actorOf(Props.create(DeadLetterActor.class));
        final ActorRef gatewayActor = system.actorOf(GatewayActor.props(lookupBus).withDispatcher("akka.pinned-dispatcher"), "gatewayActor");
        final ActorRef reqreplyActor = system.actorOf(ReqReplyActor.props( lookupBus).withDispatcher("akka.pinned-dispatcher"),"reqreplyActor");
        final ActorRef publisherActor = system.actorOf(PublisherActor.props().withDispatcher("akka.pinned-dispatcher"),"publisherActor");

        final ActorRef signalActor = system.actorOf(SignalActor.props(), "signalActor");
        final ActorRef tradecoordActor = system.actorOf(TradeCoordinatorActor.props(lookupBus), "tradeCoordActor");

        for (Map.Entry<Integer, Contract> entry : contractMap.entrySet()) {

            String actorname = entry.getValue().m_symbol + entry.getValue().m_expiry;
            if(entry.getValue().m_secType.equals(SecType.FOP.getValue()) || entry.getValue().m_secType.equals(SecType.OPT.getValue()) ){
                actorname += entry.getValue().m_strike + entry.getValue().m_right;
            }

            lookupBus.subscribe(signalActor, entry.getKey().toString());

            /*
            final ActorRef tradeActor = system.actorOf(TradeActor.props(), actorname);
            lookupBus.subscribe(tradeActor, entry.getKey().toString());
            lookupBus.subscribe(tradeActor, EventTopic.FILLS.getValue());
            lookupBus.subscribe(tradeActor, EventTopic.ORDERSTATUS.getValue());
            */


            final ActorRef timeSalesActor = system.actorOf(TimeSalesActor.props(), Prefix.TimeSales.getValue() + actorname);
            lookupBus.subscribe(timeSalesActor, Prefix.TimeSales.getValue() + entry.getKey().toString());
        }

        waitForKey();

        shutdown(system);
        system.shutdown();
        System.exit(-1);
    }


    public static void shutdown(final ActorSystem system) {
        String shutdownmanual = "\n\n***********************************************************************" +
                "\n*  Monarch SYSTEM" +
                "\n*  Manual Shutdown initiated " +
                "\n*  Shutting down " +
                "\n*  Website: http://audubontechnology.appspot.com/ " +
                "\n***********************************************************************\n\n";
        log.info(shutdownmanual);
        system.actorSelection("/user/*").tell(PoisonPill.getInstance(), ActorRef.noSender());
        try {
            Thread.sleep(2000);
        }
        catch (Exception yt) {
            yt.printStackTrace();
        }
    }
}

