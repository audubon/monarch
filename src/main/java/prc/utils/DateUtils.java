package prc.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
/**
 * User: axs
 * Date: 12/24/2014
 * Time: 7:04 AM
 */
public class DateUtils {
    public final static int HALFDAY = 43200;

    public static String today() {
        Date myDate = new Date();
        return new SimpleDateFormat("yyyyMMdd").format(myDate);
    }

    public static String todayhms() {
        Date myDate = new Date();
        return new SimpleDateFormat("yyMMddHHmmss").format(myDate);
    }

}
