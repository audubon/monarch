package prc.utils;

import java.util.Random;

/**
 * User: axs
 * Date: 12/23/2014
 * Time: 12:35 PM
 */
public class MathUtils {
    private final static Random rand = new Random();

    public static int randInt(int min, int max) {
        return rand.nextInt(max - min + 1) + min;
    }

    public static double mRound(double value, double factor) {
        return Math.round(value / factor) * factor;
    }


    //round to 2 decimals
    public static double centRound(double value) {
        return (double) Math.round(value * 100) / 100;
    }
}
