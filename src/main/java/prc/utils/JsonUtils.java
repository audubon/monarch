package prc.utils;


import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import prc.messaging.zeromq.Command;
import prc.trading.Position;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.HashMap;

/**
 * User: axs
 * Date: 12/24/2014
 * Time: 7:05 AM
 */
public final class JsonUtils {
    private static ObjectMapper mapper = new ObjectMapper();


    public static void write(Object obj, String FileName) throws IOException {
        mapper.writeValue(new File(FileName), obj);
    }


    public static String write(Object obj) throws IOException {
        return mapper.writeValueAsString(obj);
    }


    public static Map<String, Double> readToMap(String json) throws IOException {
        return mapper.readValue(json, new TypeReference<HashMap<String, Double>>() {
        });
    }

    public static Map<String, Position> readToPosition(String json) throws IOException {
        return mapper.readValue(json, new TypeReference<HashMap<String, Position>>() {
        });
    }

    public static Command readToCommand(String json) throws IOException {
        Map<String, String> command = mapper.readValue(json, new TypeReference<HashMap<String, String>>() {
        });
        return new Command(command);
    }
}
