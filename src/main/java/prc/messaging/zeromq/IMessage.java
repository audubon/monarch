package prc.messaging.zeromq;

/**
 * User: axs
 * Date: 12/29/2014
 * Time: 4:48 PM
 */
public interface IMessage {

    String getTopic();

    long getTimestamp();

    Object getPayload();

    String toJson(Object object) throws Exception;
}

