package prc.messaging.zeromq;



import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;


/**
 * User: axs
 * Date: 12/29/2014
 * Time: 8:37 AM
 */
public final class Command implements Serializable {
    private static final long serialVersionUID = 4029272298628718147L;
    private final CommandType action;
    private final String target;
    private final String argument;

    public Command(Map<String, String> input) {
        String strAction = input.getOrDefault(CommandInput.ACTION.getValue(), "NONE");
        action = CommandType.getInstanceForValue(strAction);

        target = input.getOrDefault(CommandInput.TARGET.getValue(), "NONE");
        argument = input.getOrDefault(CommandInput.ARGUMENT.getValue(), "NONE");
    }

    public CommandType getAction() {
        return action;
    }

    public String getTarget() {
        return target;
    }

    public String getArgument() {
        return argument;
    }



    public static Command create(String target, CommandType action) {
        Map<String, String> inp = new HashMap<>();
        inp.put(CommandInput.TARGET.getValue(), target);
        inp.put(CommandInput.ACTION.getValue(), action.toString());
        return new Command(inp);
    }

}
