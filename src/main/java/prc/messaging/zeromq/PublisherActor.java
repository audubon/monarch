package prc.messaging.zeromq;


import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.japi.Creator;
import org.zeromq.ZMQ;
import prc.messaging.zeromq.IMessage;
import prc.ConfigReader;
import prc.trading.conf.MonarchConf;

/**
 * User: axs
 * Date: 12/29/2014
 * Time: 4:47 PM
 */
public class PublisherActor extends UntypedActor {
    private final String endpoint;
    private final LoggingAdapter log = Logging.getLogger(getContext().system(), "monarch");
    private final String myname = getSelf().toString();
    private ZMQ.Socket publisher;
    private ZMQ.Context context;

    public static Props props() {
        return Props.create(new Creator<PublisherActor>() {
            private static final long serialVersionUID = 1L;

            @Override
            public PublisherActor create() throws Exception {
                return new PublisherActor();
            }
        });
    }

    public PublisherActor() {
        MonarchConf etc = ConfigReader.readMonarchConfFile();
        endpoint = etc.getZMQPubSubEndpoint();
    }


    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof IMessage) {
            IMessage msg = (IMessage) message;
            try {
                String json = msg.toJson(msg);
                publish(msg.getTopic(), json);
            }
            catch (Exception ert) {
                log.warning("WARN {} {}", ert.getMessage(), myname);
            }
        }
        else
            unhandled(message);
    }


    private void publish(String topic, String msg) {
        publisher.sendMore(topic);
        publisher.send(msg);
    }


    @Override
    public void preStart() {
        log.info("prestart: {}", myname);
        context = ZMQ.context(1);
        publisher = context.socket(ZMQ.PUB);
        publisher.bind(endpoint);
        publisher.setLinger(1000);
        publisher.setSndHWM(1000);
        publisher.setRcvHWM(1000);
    }

    @Override
    public void preRestart(Throwable reason, scala.Option<Object> message) {
        log.info("preREstart: {}", myname);
        for (ActorRef each : getContext().getChildren()) {
            getContext().unwatch(each);
            getContext().stop(each);
        }
        postStop();
    }

    @Override
    public void postStop() {
        log.info("postStop: {}", myname);
        publisher.unbind(endpoint);
        publisher.close();
        context.term();
        try {
            context.close();
        }
        catch (Exception ert) {
            log.error(ert.getMessage());
        }
    }
}
