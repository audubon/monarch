package prc.messaging.zeromq;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * User: axs
 * Date: 1/6/2015
 * Time: 2:04 PM
 */
public enum Topics {
    POSITIONS("POSITIONS"),
    LIQUID("LIQUID"),
    MARKETMAKER("MARKETMAKER"),
    HEDGE("HEDGE"),
    NAV("NAV"),
    EVERYTHING(""),
    SIGNAL("SIG");

    private static final Map<String, Topics> mValueMap;
    private final String topic;

    Topics(String s) {
        this.topic = s;
    }


    public String getValue() {
        return topic;
    }


    static Topics getInstanceForValue(String inValue) {
        return mValueMap.get(inValue);
    }


    static {
        Map<String, Topics> table = new HashMap<>();
        for (Topics ot : values()) {
            table.put(ot.getValue(), ot);
        }
        mValueMap = Collections.unmodifiableMap(table);
    }

}
