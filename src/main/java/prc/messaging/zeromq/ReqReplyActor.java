package prc.messaging.zeromq;


import static akka.pattern.Patterns.ask;


import akka.util.Timeout;
import prc.ConfigReader;
import prc.messaging.akka.EventTopic;
import prc.messaging.akka.LookupBusImpl;
import prc.messaging.akka.MsgEnvelope;
import prc.messaging.akka.notifications.AnnouncementCodes;
import prc.messaging.zeromq.Command;
import prc.messaging.zeromq.CommandType;
import prc.trading.conf.MonarchConf;
import scala.concurrent.Await;
import scala.concurrent.Future;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.japi.Creator;
import org.zeromq.ZMQ;
import prc.utils.JsonUtils;
import scala.concurrent.duration.Duration;

/**
 * User: axs
 * Date: 12/29/2014
 * Time: 4:35 PM
 */
public class ReqReplyActor extends UntypedActor {
    private final String endpoint;
    private final LoggingAdapter log = Logging.getLogger(getContext().system(), "monarch");
    private final String myname = getSelf().toString();
    private Thread consumerThread;
    private ZMQ.Socket consumer;
    private ZMQ.Context context;
    private Timeout timeout;
    private final LookupBusImpl lookupBus;


    public static Props props(final LookupBusImpl lookupBus) {
        return Props.create(new Creator<ReqReplyActor>() {
            private static final long serialVersionUID = 1L;

            @Override
            public ReqReplyActor create() throws Exception {
                return new ReqReplyActor(lookupBus);
            }
        });
    }

    public ReqReplyActor(LookupBusImpl lookupBus) {
        this.lookupBus = lookupBus;
        MonarchConf etc = ConfigReader.readMonarchConfFile();
        endpoint = etc.getZMQReqReplyEndpoint();
    }


    private void sendDone() {
        try {
            consumer.send(JsonUtils.write("DONE"), 0);
        }
        catch (Exception yt) {
            yt.printStackTrace();
        }
    }

    public void consume() {
        ZMQ.Poller poller = new ZMQ.Poller(1000);
        poller.register(consumer);
        while (!Thread.currentThread().isInterrupted()) {
            poller.poll();
            String topic = null;
            String contents = null;

            try {
                topic = consumer.recvStr(ZMQ.NOBLOCK);
                contents = consumer.recvStr(ZMQ.NOBLOCK);
            }
            catch (IllegalStateException ise) {
                ise.printStackTrace();
                //XXX
                /*
                if we get an exception because we try to request or reply twice in a row send the Actor
                 the exception which will in turn restart the actor
                 */
                getSelf().tell(ise, ActorRef.noSender());
                break;
            }

            Command command = null;
            try {
                command = JsonUtils.readToCommand(contents);
            }
            catch (Exception ert) {
                log.error(ert.getMessage());
            }

            try {
                log.info("{} {} {}", myname, topic, contents);
            }
            catch (Exception ert) {
                ert.printStackTrace();
            }

            if (command.getAction() == CommandType.DISABLEALL || command.getAction() == CommandType.ENABLE || command.getAction().isLiquidCommand()) {
                getContext().actorSelection("/user/" + command.getTarget()).tell(command, getSelf());
                sendDone();
            }

        }
    }


    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof IllegalStateException) {
            throw new IllegalStateException();
        }
    }


    @Override
    public void preStart() {
        log.info("prestart: {}", myname);
        context = ZMQ.context(1);
        consumer = context.socket(ZMQ.REP);
        consumer.setReceiveTimeOut(900);
        consumer.setLinger(1000);
        consumer.setSndHWM(1000);
        consumer.setRcvHWM(1000);
        consumer.bind(endpoint);
        timeout = new Timeout(Duration.create(9, "seconds"));
        consumerThread = new Thread(this::consume);
        consumerThread.start();
    }

    @Override
    public void preRestart(Throwable reason, scala.Option<Object> message) {
        log.info("preREstart: {}", myname);
        for (ActorRef each : getContext().getChildren()) {
            getContext().unwatch(each);
            getContext().stop(each);
        }
        postStop();
    }

    @Override
    public void postStop() {
        log.info("postStop: {}", myname);
        consumer.close();
        context.term();
        consumerThread.interrupt();
    }
}
