package prc.messaging.zeromq;

import java.io.DataInput;
import java.io.FilterReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * User: axs
 * Date: 12/29/2014
 * Time: 8:39 AM
 */
public enum CommandType {
    SUSPEND("SUSPEND"),
    READY("READY"),
    ABOUT("ABOUT"),
    ALGOCHANGE("ALGOCHANGE"),
    ALGOCONFIG("ALGOCONFIG"),
    ALGOSAVE("ALGOSAVE"),
    CANCELALL("CANCELALL"),
    ENABLE("ENABLE"),
    DISABLEALL("DISABLEALL"),
    CLIENTCONNECTED("CLIENTCONNECTED"),
    OFFERLIQUIDITY("OFFERLIQUIDITY"),
    ACTIVATELIQUIDITY("ACTIVATELIQUIDITY"),
    DEACTIVATELIQUIDITY("DEACTIVATELIQUIDITY"),
    DISABLELIQUIDITY("DISABLELIQUIDITY"),
    ATTACK("ATTACK"),
    HITLIFT("HITLIFT"),
    RETREAT("RETREAT"),
    RESET_ACTIVITY("RESET_ACTIVITY"),
    EXTERNALFILL("EXTERNALFILL");


    public static final EnumSet<CommandType> LIQUIDCOMMANDS = EnumSet.of(CommandType.HITLIFT, CommandType.ATTACK, CommandType.RETREAT);

    public final boolean isLiquidCommand() {
        return LIQUIDCOMMANDS.contains(this);
     }

    private static final Map<String, CommandType> mValueMap;
    private final String topic;

    CommandType(String s) {
        this.topic = s;
    }


    public String getValue() {
        return topic;

    }


    static CommandType getInstanceForValue(String inValue) {
        return mValueMap.get(inValue);
    }


    static {
        Map<String, CommandType> table = new HashMap<>();
        for (CommandType ot : values()) {
            table.put(ot.getValue(), ot);
        }
        mValueMap = Collections.unmodifiableMap(table);
    }

}

