package prc.messaging.zeromq;

import org.codehaus.jackson.map.ObjectMapper;

/**
 * User: axs
 * Date: 1/6/2015
 * Time: 2:03 PM
 */
public class Message implements IMessage {
    private final static ObjectMapper mapper = new ObjectMapper();
    private final String topic;
    private final Object payload;
    private final long timestamp;


    public Message(Object payload) {
        this(Topics.SIGNAL.getValue(), payload);
    }

    public Message(String topic, Object payload) {
        this.topic = topic;
        this.payload = payload;
        this.timestamp = System.currentTimeMillis();
    }

    @Override
    public String getTopic() {
        return this.topic;
    }

    @Override
    public Object getPayload() {
        return this.payload;
    }

    public long getTimestamp() {
        return timestamp;
    }

    @Override
    public String toJson(Object object) throws Exception {
        //return mapper.writeValueAsString(new Object[]{this.timestamp,this.payload});
        return mapper.writeValueAsString(this.payload);
    }
}
