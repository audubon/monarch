package prc.messaging.zeromq;


import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * User: axs
 * Date: 12/29/2014
 * Time: 8:38 AM
 */
public enum CommandInput {
    ACTION("ACTION"),
    TARGET("TARGET"),
    ARGUMENT("ARGUMENT");

    private static final Map<String, CommandInput> mValueMap;
    private final String topic;

    CommandInput(String s) {
        this.topic = s;
    }


    public String getValue() {
        return topic;
    }


    static CommandInput getInstanceForValue(String inValue) {
        return mValueMap.get(inValue);
    }


    static {
        Map<String, CommandInput> table = new HashMap<>();
        for (CommandInput ot : values()) {
            table.put(ot.getValue(), ot);
        }
        mValueMap = Collections.unmodifiableMap(table);
    }


}
