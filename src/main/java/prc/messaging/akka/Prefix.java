package prc.messaging.akka;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * User: axs
 * Date: 1/2/2015
 * Time: 12:02 PM
 */
public enum Prefix {
    TimeSales("TS_")   ;


    private static final Map<String, Prefix> mValueMap;
    private final String otype;

    Prefix(String ot) {
        this.otype=ot;
    }


    public String getValue() {
        return otype;
    }

    static Prefix getInstanceForValue(String inValue) {
        Prefix ot = mValueMap.get(inValue);
        return ot;
    }


    static {
        Map<String, Prefix> table = new HashMap<String,Prefix>();
        for( Prefix ot: values() ) {
            table.put(ot.getValue(), ot);
        }
        mValueMap = Collections.unmodifiableMap(table);
    }
}
