package prc.messaging.akka;

/**
 * User: axs
 * Date: 12/22/2014
 * Time: 3:16 PM
 */
public class MsgEnvelope {
    public final String topic;
    public final Object payload;

    public MsgEnvelope(String topic, Object payload) {
        this.topic = topic;
        this.payload = payload;
    }
}
