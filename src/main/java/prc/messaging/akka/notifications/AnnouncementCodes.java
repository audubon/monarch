package prc.messaging.akka.notifications;



import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


public enum AnnouncementCodes {
    FIXCONNECTED("FIXCONNECTED"),
    FIXDISCONNECTED("FIXDISCONNECTED"),
    DESTINATION_DOWN("DESTINATION_DOWN"),
    REJECTED("REJECTED"),
    DATADOWN("DATADOWN"),
    DATAUP("DATAUP"),
    ACTIVITY_LIMIT("ACTIVITY_LIMIT"),
    ACTIVITY_RESET("ACTIVITY_RESET"),
    CONNECTED_CLIENT("CONNECTED_CLIENT"),
    STATUS("STATUS"),
    PUBNAV("PUBNAV");


    private static final Map<String, AnnouncementCodes> mValueMap;
    private final String topic;

    AnnouncementCodes(String s) {
        this.topic = s;
    }


    public String getValue() {
        return topic;
    }


    static AnnouncementCodes getInstanceForValue(String inValue) {
        return mValueMap.get(inValue);
    }


    static {
        Map<String, AnnouncementCodes> table = new HashMap<>();
        for (AnnouncementCodes ot : values()) {
            table.put(ot.getValue(), ot);
        }
        mValueMap = Collections.unmodifiableMap(table);
    }

}
