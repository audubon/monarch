package prc.ib;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * User: axs
 * Date: 12/22/2014
 * Time: 3:46 PM
 */
public class OrderIDGenerator {
    private static AtomicInteger orderid = new AtomicInteger(101);


    public static int getAndIncrement() {
        return orderid.getAndIncrement();
    }

    public static int getOrderID() {
        return orderid.get();
    }

    public static void setOrderID(int val) {
        orderid.set(val);
    }
}

