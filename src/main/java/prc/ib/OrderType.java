package prc.ib;

import java.util.Map;
import java.util.HashMap;
import java.util.Collections;


/**
 * User: axs
 * Date: 12/22/2014
 * Time: 3:47 PM
 */
public enum OrderType {
     LIMIT("LMT")
    ,MARKET("MKT")
    ,MARKETCLOSE("MKTCLS")
    ,LIMITCLOSE("LMTCLS")
    ,PEGGED("PEGMKT")
    ,SCALE("SCALE")
    ,STOP("STP")
    ,STOPLIMIT("STPLMT")
    ,TRAIL("TRAIL")
    ,RELATIVE("REL")
    ,VWAP("VWAP")
    ,TRAILLIMIT("TRAILLIMIT")   ;


    private static final Map<String, OrderType> mValueMap;
    private final String otype;

    OrderType(String ot) {
        this.otype=ot;
    }


    public String getValue() {
        return otype;
    }

    static OrderType getInstanceForValue(String inValue) {
        OrderType ot = mValueMap.get(inValue);
        return ot;
    }


    static {
        Map<String, OrderType> table = new HashMap<String,OrderType>();
        for( OrderType ot: values() ) {
            table.put(ot.getValue(), ot);
        }
        mValueMap = Collections.unmodifiableMap(table);
    }



}

