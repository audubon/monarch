package prc.ib;

import com.ib.client.Contract;
import com.ib.client.Execution;

import java.io.Serializable;

/**
 * User: axs
 * Date: 12/24/2014
 * Time: 8:49 AM
 */
public final class Fill implements Serializable {
    private static final long serialVersionUID = 309657481241L;

    private final Contract contract;
    private final Execution execution;

    public Fill(Contract contract, Execution execution) {
        this.contract = contract;
        this.execution = execution;
    }

    public Contract getContract() {
        return contract;
    }

    public Execution getExecution() {
        return execution;
    }


}
