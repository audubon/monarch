package prc.ib;


import com.ib.client.ComboLeg;
import com.ib.client.Order;
import com.ib.client.Execution;


/**
 * User: axs
 * Date: 12/22/2014
 * Time: 3:44 PM
 */
public class OrderBuilder {

    private Order order;

    private static final int out_multiplier = 3;
    private static final int stp_multiplier = 2;


    public OrderBuilder() {
        this.order = new Order();

        this.order.m_clientId = 0; //Configuration.getClientId();
        this.order.m_account = "";//Configuration.getAccount() ;
        //this.order.m_orderId = OrderID.getAndIncrement();
        this.order.m_orderId = 1;
    }


    public OrderBuilder withPrice(final double price) {
        this.order.m_lmtPrice = price;
        return this;
    }


    public OrderBuilder withSide(final Side action) {
        this.order.m_action = action.getValue();
        return this;
    }


    public OrderBuilder withTIF(final TimeInForce tif) {
        this.order.m_tif = tif.getValue();
        return this;
    }

    public OrderBuilder withOrderType(final OrderType tp) {
        order.m_orderType = tp.getValue();
        return this;
    }

    public OrderBuilder withParentId(final int id) {
        order.m_parentId = id;
        return this;
    }


    public OrderBuilder withOcaGroup(final String ocaGroup) {
        order.m_ocaGroup = ocaGroup;
        return this;
    }



    public OrderBuilder withQuantity(final int qty) {
        order.m_totalQuantity = qty;
        return this;
    }


    //YYYYMMDD hh:mm:ss
    public OrderBuilder withGoodTillDate(final String datestring) {
        order.m_goodTillDate = datestring;
        return this;
    }


    public Order create() {
        return this.order;
    }


    public static Order createLimitOrder(final double price, final Side side) {
        return new OrderBuilder()
                .withOrderType(OrderType.LIMIT)
                .withSide(side)
                .withQuantity(1)
                .withPrice(price)
                .withTIF(TimeInForce.DAY)
                .create();
    }


///////

    public static Order createOrder(final Side action) {

        Order order = new Order();
        order.m_action = action.getValue();
        order.m_orderType = OrderType.LIMIT.getValue();
        order.m_lmtPrice = 56.32;
        order.m_tif = TimeInForce.DAY.getValue();
        order.m_totalQuantity = 1;
        order.m_clientId = 0;
        order.m_permId = 0;
        order.m_auxPrice = 0;
        order.m_transmit = true;
        order.m_orderId = OrderIDGenerator.getAndIncrement();

        return (order);
    }


    public static Order createOrder() {
        Order order = new Order();
        order.m_action = Side.BUY.getValue();
        order.m_orderType = OrderType.LIMIT.getValue();
        order.m_lmtPrice = 56.37;


        order.m_tif = TimeInForce.DAY.getValue();
        order.m_totalQuantity = 12;
        order.m_clientId = 0;
        //order.m_account= Configuration.getAccount();
        order.m_orderId = OrderIDGenerator.getAndIncrement();

        return (order);
    }


    public static Order[] createBracket(final Order order, final double tick) {

        Order border = new Order();
        border.m_action = order.m_action.equals(Side.BUY.getValue())
                ? Side.SELL.getValue()
                : Side.BUY.getValue();
        border.m_orderType = OrderType.LIMIT.getValue();
        border.m_lmtPrice = border.m_action.equals(Side.BUY.getValue())
                ? order.m_lmtPrice - tick
                : order.m_lmtPrice + tick;
        border.m_tif = TimeInForce.DAY.getValue();
        border.m_totalQuantity = 1;
        border.m_clientId = 0;//Configuration.getClientId();
        //border.m_account = Configuration.getAccount();
        border.m_orderId = 0;//OrderIDGenerator.getAndIncrement();
        border.m_parentId = 0; //order.m_orderId;


        Order sorder = new Order();
        sorder.m_action = order.m_action.equals(Side.BUY.getValue()) ? Side.SELL.getValue() : Side.BUY.getValue();
        sorder.m_orderType = OrderType.STOP.getValue();
        sorder.m_auxPrice = sorder.m_action.equals(Side.BUY.getValue())
                ? order.m_lmtPrice + tick
                : order.m_lmtPrice - tick ;
        sorder.m_tif = TimeInForce.DAY.getValue();
        sorder.m_totalQuantity = 1;
        sorder.m_clientId = 0;//Configuration.getClientId();
        //sorder.m_account= Configuration.getAccount();
        sorder.m_orderId = 0; //OrderIDGenerator.getAndIncrement();
        sorder.m_parentId = 0; //order.m_orderId;

        return (new Order[]{border, sorder});
    }


    public static Order createOrder(final Execution order, final double tick, final Quote qt) {

        int oid = OrderIDGenerator.getAndIncrement();

        Order border = new Order();
        border.m_action = order.m_side.equals("BOT") ? Side.SELL.getValue() : Side.BUY.getValue();
        border.m_orderType = OrderType.LIMIT.getValue();
        border.m_lmtPrice = border.m_action.equals(Side.BUY.getValue())
                ? qt.getBid()
                : qt.getAsk();
        border.m_tif = TimeInForce.DAY.getValue();
        border.m_totalQuantity = order.m_shares;
        border.m_clientId = 0;//Configuration.getClientId();
        //border.m_account= Configuration.getAccount();
        border.m_orderId = oid;


        return border;
    }


    public static Order createOrder(final Execution order, final Quote qt, int offset, double tick) {
        Order border = new Order();
        border.m_action = order.m_side.equals("BOT") ? Side.SELL.getValue() : Side.BUY.getValue();
        border.m_orderType = OrderType.LIMIT.getValue();
        border.m_lmtPrice = border.m_action.equals(Side.BUY.getValue())
                ? order.m_price - (offset * tick)
                : order.m_price + (offset * tick);
        border.m_tif = TimeInForce.DAY.getValue();
        border.m_totalQuantity = order.m_shares;
        border.m_clientId = 0;//Configuration.getClientId();
        //border.m_account= Configuration.getAccount();
        border.m_orderId = OrderIDGenerator.getAndIncrement();


        return border;
    }

    public static Order[] createOCO(final Execution order, final double tick, final Quote qt, final String OCgroup) {

        int oid = 0;//OrderID.getAndIncrement();

        Order border = new Order();
        border.m_action = order.m_side.equals(Side.BOT.getValue()) ? Side.SELL.getValue() : Side.BUY.getValue();
        border.m_orderType = OrderType.LIMIT.getValue();
        border.m_lmtPrice = border.m_action.equals(Side.BUY.getValue())
                ? qt.getBid() - tick * out_multiplier
                : qt.getAsk() + tick * out_multiplier;
        border.m_tif = TimeInForce.DAY.getValue();
        border.m_totalQuantity = order.m_shares;
        border.m_clientId = 0;//Configuration.getClientId();
        //border.m_account= Configuration.getAccount();
        border.m_orderId = oid;
        border.m_ocaGroup = OCgroup;

        Order sorder = new Order();
        sorder.m_action = border.m_action; //order.m_side.equals("BOT")? Side.SELL.getValue() : Side.BUY.getValue();
        sorder.m_orderType = OrderType.STOP.getValue();
        sorder.m_auxPrice = sorder.m_action.equals(Side.BUY.getValue())
                ? qt.getAsk() + tick * stp_multiplier
                : qt.getBid() - tick * stp_multiplier;
        sorder.m_tif = TimeInForce.DAY.getValue();
        sorder.m_totalQuantity = order.m_shares;
        sorder.m_clientId = 0;//Configuration.getClientId();
        //sorder.m_account= Configuration.getAccount();
        sorder.m_orderId = 0; //OrderID.getAndIncrement();
        sorder.m_ocaGroup = OCgroup;

        return (new Order[]{border, sorder});
    }
    /*

    public static Order[] createSpreadOrder( Side side, Spread spread ){
        Order buyorder =  createOrder( spread.getFrontQuote() , side , spread.getFrontSize() , 0);
        Order sellorder =  createOrder( spread.getBackQuote() , side.getFlip() , spread.getBackSize(), 0);

        return new Order[]{buyorder, sellorder };
    }




   */

}




