package prc.ib;

import java.io.Serializable;

/**
 * User: axs
 * Date: 12/29/2014
 * Time: 11:43 AM
 */
public class BracketOrderRequest implements Serializable {
    private static final long serialVersionUID = 357485398551L;
    private final OrderRequest[] orderRequestList;

    public BracketOrderRequest(OrderRequest[] orderRequestList) {
        this.orderRequestList = orderRequestList;
    }


    public OrderRequest[] getOrderRequestList() {
        return orderRequestList;
    }

    public OrderRequest getParentOrderRequest() {
        return orderRequestList[0];
    }


}
