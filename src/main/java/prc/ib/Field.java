package prc.ib;

/**
 * User: axs
 * Date: 12/23/2014
 * Time: 8:56 AM
 */
public enum Field {
    BID_SIZE, BID, ASK, ASK_SIZE, LAST, LAST_SIZE, HIGH, LOW, VOLUME, CLOSE;

}
