package prc.ib;

import java.util.Map;
import java.util.HashMap;
import java.util.Collections;


/**
 * User: axs
 * Date: 12/22/2014
 * Time: 3:49 PM
 */
public enum TimeInForce {
    DAY("DAY"),
    GTC("GTC"),
    IOC("IOC"),
    GTD("GTD");


    private static final Map<String, TimeInForce> mValueMap;
    private final String tif;

    TimeInForce(String s) {
        this.tif = s;
    }


    public String getValue() {
        return tif;
    }


    static TimeInForce getInstanceForValue(String inValue) {
        TimeInForce ot = mValueMap.get(inValue);
        return ot;
    }


    static {
        Map<String, TimeInForce> table = new HashMap<String, TimeInForce>();
        for (TimeInForce ot : values()) {
            table.put(ot.getValue(), ot);
        }
        mValueMap = Collections.unmodifiableMap(table);
    }

}
