package prc.ib;


import java.util.Map;
import java.util.HashMap;
import java.util.Collections;


/**
 * User: axs
 * Date: 12/22/2014
 * Time: 3:45 PM
 */
public enum Side {
    BUY("BUY"), SELL("SELL"), BOT("BOT"), SLD("SLD");

    private static final Map<String, Side> mValueMap;
    private final String side;

    Side(String s) {
        this.side = s;
    }


    public String getValue() {
        return side;
    }


    public Side getFlip() {
        return this == Side.BUY ? Side.SELL : Side.BUY;
    }


    static Side getInstanceForValue(String inValue) {
        Side ot = mValueMap.get(inValue);
        return ot;
    }


    static {
        Map<String, Side> table = new HashMap<String, Side>();
        for (Side ot : values()) {
            table.put(ot.getValue(), ot);
        }
        mValueMap = Collections.unmodifiableMap(table);
    }

}

