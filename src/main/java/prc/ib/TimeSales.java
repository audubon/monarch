package prc.ib;

import java.io.Serializable;

/**
 * User: axs
 * Date: 1/2/2015
 * Time: 11:40 AM
 */
public class TimeSales implements Serializable {
    private static final long serialVersionUID = 1L;
    private final double price;
    private final int size;
    private final long time;
    private final int volume;
    private final double vwap;
    private final boolean flag;

    //53.41;16;1420219650605;150767;53.13095473;false

    public TimeSales(String delimited) {
        String[] data = delimited.split(";");
        price = Double.parseDouble(data[0]);
        size = Integer.parseInt(data[1]);
        time = Long.parseLong(data[2]);
        volume = Integer.parseInt(data[3]);
        vwap = Double.parseDouble(data[4]);
        flag = data[5].equals("true") ? true : false;
    }

    public double getPrice() {
        return price;
    }

    public int getSize() {
        return size;
    }

    public long getTime() {
        return time;
    }

    public int getVolume() {
        return volume;
    }

    public double getVwap() {
        return vwap;
    }

    public boolean isFlag() {
        return flag;
    }


    @Override
    public String toString() {
        return "TimeSales{" +
                "price=" + price +
                ", size=" + size +
                ", time=" + time +
                ", volume=" + volume +
                ", vwap=" + vwap +
                ", flag=" + flag +
                '}';
    }
}
