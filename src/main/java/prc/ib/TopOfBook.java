package prc.ib;

import java.util.concurrent.ConcurrentHashMap;

/**
 * User: axs
 * Date: 12/22/2014
 * Time: 4:34 PM
 */
public class TopOfBook {
    //symbol --> Quote
    private final ConcurrentHashMap<Integer, Quote> topofbook;
    private static final TopOfBook INSTANCE = new TopOfBook();


    private TopOfBook() {
        this.topofbook = new ConcurrentHashMap<Integer, Quote>();
    }

    public static TopOfBook getInstance() {
        return INSTANCE;
    }

    public void setBid(Integer symid, double bidprice) {
        try {
            this.topofbook.get(symid).setBid(bidprice);
        }
        catch (NullPointerException npe) {
            npe.printStackTrace();
        }
    }

    public void setAsk(Integer symid, double askprice) {
        try {
            this.topofbook.get(symid).setAsk(askprice);
        }
        catch (NullPointerException npe) {
            npe.printStackTrace();
        }
    }


    public void setLast(Integer symid, double lastprice) {
        try {
            this.topofbook.get(symid).setLast(lastprice);
        }
        catch (NullPointerException npe) {
            npe.printStackTrace();
        }
    }

    public void setLastQty(Integer symid, int lastsize) {
        try {
            this.topofbook.get(symid).setLastQty(lastsize);
        }
        catch (NullPointerException npe) {
            npe.printStackTrace();
        }
    }

    public void setAskQty(Integer symid, int askprice) {
        try {
            this.topofbook.get(symid).setAskQty(askprice);
        }
        catch (NullPointerException npe) {
            npe.printStackTrace();
        }
    }

    public void setBidQty(Integer symid, int askprice) {
        try {
            this.topofbook.get(symid).setBidQty(askprice);
        }
        catch (NullPointerException npe) {
            npe.printStackTrace();
        }
    }

    public boolean contains(Integer symid) {
        return this.topofbook.containsKey(symid);
    }

    public void addQuote(Quote qt) {
        this.topofbook.put(qt.getContractID(), qt);
    }

    public Quote getQuote(Integer symid) {
        if (this.topofbook.containsKey(symid))
            return this.topofbook.get(symid);
        else {
            return null;
        }
    }

    public void removeQuote(Quote qt) {
        this.topofbook.remove(qt.getContractID());
    }
}
