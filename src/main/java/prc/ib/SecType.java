package prc.ib;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * User: axs
 * Date: 12/24/2014
 * Time: 7:25 AM
 */
public enum SecType {
     STK("STK")
    ,OPT("OPT")
    ,FUT("FUT")
    ,IND("IND")
    ,FOP("FOP")
    ,CASH("CASH")
    ,BAG("BAG")
    ,NEWS("NEWS");


   private static final Map<String, SecType> mValueMap;
   private final String otype;

    SecType(String ot) {
       this.otype=ot;
   }


   public String getValue() {
       return otype;
   }

   static SecType getInstanceForValue(String inValue) {
       SecType ot = mValueMap.get(inValue);
       return ot;
   }


   static {
       Map<String, SecType> table = new HashMap<String,SecType>();
       for( SecType ot: values() ) {
           table.put(ot.getValue(), ot);
       }
       mValueMap = Collections.unmodifiableMap(table);
   }



}
