package prc.ib;

import java.io.Serializable;

/**
 * User: axs
 * Date: 12/29/2014
 * Time: 2:01 PM
 */
public class CancelReplace implements Serializable{
    private static final long serialVersionUID = 3038511581241L;
    private final OrderRequest orderRequest;

    public CancelReplace(OrderRequest orderRequest) {
        this.orderRequest = orderRequest;
    }

    public OrderRequest getOrderRequest() {
        return orderRequest;
    }
}
