package prc.ib;

import com.ib.client.Contract;
import com.ib.client.Order;

import java.io.Serializable;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * User: axs
 * Date: 12/23/2014
 * Time: 12:20 PM
 */
public final class OrderRequest implements Serializable {
    private static final long serialVersionUID = 357486441L;
    private static AtomicInteger atomicInteger = new AtomicInteger(101);

    private final Contract contract;
    private final Order order;
    private final int orderReqID;
    private final int contractID;

    public OrderRequest(Contract contract, Order order,int contractID) {
        this.contract = contract;
        this.order = order;
        this.contractID = contractID;
        this.orderReqID = atomicInteger.getAndIncrement();

    }

    public Contract getContract() {
        return contract;
    }

    public Order getOrder() {
        return order;
    }

    public int getOrderReqID() {
        return orderReqID;
    }


    public int getContractID() {
        return contractID;
    }
}
