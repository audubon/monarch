package prc.ib;

import java.io.Serializable;

/**
 * User: axs
 * Date: 1/26/2015
 * Time: 11:33 AM
 */
public class Cancel implements Serializable {
    private static final long serialVersionUID = 345871581241L;
    private final int orderid;

    public Cancel(int orderid) {
        this.orderid = orderid;
    }

    public int getOrderid() {
        return orderid;
    }
}
