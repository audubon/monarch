package prc.ib;

import com.ib.client.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import prc.messaging.akka.EventTopic;
import prc.messaging.akka.LookupBusImpl;
import prc.messaging.akka.MsgEnvelope;
import prc.messaging.akka.Prefix;
import prc.trading.ContractLookUp;
import prc.trading.enums.TradeState;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Vector;


/**
 * User: axs
 * Date: 12/22/2014
 * Time: 4:28 PM
 */
public final class Connection implements EWrapper, Constants {
    private static Logger logger = LoggerFactory.getLogger("ib");
    private TopOfBook topOfBook = TopOfBook.getInstance();
    final ContractLookUp contractLookUp = ContractLookUp.getInstance();
    private boolean faError;
    private EClientSocket m_client;
    private final LookupBusImpl lookupBus;

    public Connection(final LookupBusImpl lookupBus) {
        this.lookupBus = lookupBus;
    }

    public void conn(int clientID, String ipaddress) {
        m_client = new EClientSocket(this);
        m_client.eConnect(ipaddress, 7496, clientID);
        m_client.setServerLogLevel(4);

        if (m_client.isConnected()) {
            logger.info("Connected to Tws server version " +
                    m_client.serverVersion() + " at " +
                    m_client.TwsConnectionTime());
        }
        try {
            Thread.sleep(2000);
        }
        catch (Exception yt) {
            yt.printStackTrace();
        }

    }

    public void cancelOrder(final int orderID){
        this.m_client.cancelOrder(orderID);
    }

    public void placeOrder(final OrderRequest orderRequest) {
        this.m_client.placeOrder(orderRequest.getOrder().m_orderId, orderRequest.getContract(), orderRequest.getOrder());
    }

    public void placeOrder(final OrderRequest[] orderRequests) {
        for (OrderRequest orderRequest : orderRequests) {
            this.m_client.placeOrder(orderRequest.getOrder().m_orderId, orderRequest.getContract(), orderRequest.getOrder());
        }
    }

    public void reqConDetails(final Map<Integer, Contract> instruments) {
        for (Map.Entry<Integer, Contract> entry : instruments.entrySet()) {
            Contract i = entry.getValue();
            Integer ticker = entry.getKey();
            logger.info("reqConDetails called " + ticker + " " + i.m_symbol + " " + i.m_conId);
            m_client.reqContractDetails(ticker, i);
        }
    }

    public void getMktDataOnly(final Map<Integer, Contract> inst) {

        for (Map.Entry<Integer, Contract> entry : inst.entrySet()) {
            logger.info("getMktDataOnly called " + entry.getKey() + " " + entry.getValue().m_symbol);
            m_client.reqMktData(entry.getKey(), entry.getValue(), "233", false);
        }
    }

    @Override
    public void tickPrice(int tickerId, int field, double price, int canAutoExecute) {
        if (topOfBook.contains(tickerId)) {
            if (Field.BID.ordinal() == field) {
                topOfBook.setBid(tickerId, price);
            }
            else if (Field.ASK.ordinal() == field) {
                topOfBook.setAsk(tickerId, price);
            }
            else if (Field.LAST.ordinal() == field) {
                topOfBook.setLast(tickerId, price);
            }
        }
        else {
            Quote quote = new Quote(tickerId);
            if (Field.BID.ordinal() == field) {
                quote.setBid(price);
            }
            else if (Field.ASK.ordinal() == field) {
                quote.setAsk(price);
            }
            else if (Field.LAST.ordinal() == field) {
                quote.setLast(price);
            }
            topOfBook.addQuote(quote);
        }


        this.lookupBus.publish(new MsgEnvelope(Integer.toString(tickerId), topOfBook.getQuote(tickerId)));
    }


    @Override
    public void tickSize(int tickerId, int field, int size) {
        if (topOfBook.contains(tickerId)) {
            if (Field.BID_SIZE.ordinal() == field) {
                topOfBook.setBidQty(tickerId, size);
            }
            else if (Field.ASK_SIZE.ordinal() == field) {
                topOfBook.setAskQty(tickerId, size);
            }
            else if (Field.LAST_SIZE.ordinal() == field) {
                topOfBook.setLastQty(tickerId, size);
            }
        }
        else {
            Quote quote = new Quote(tickerId);
            if (Field.ASK_SIZE.ordinal() == field) {
                quote.setAskQty(size);
            }
            else if (Field.BID_SIZE.ordinal() == field) {
                quote.setBidQty(size);
            }
            else if (Field.LAST_SIZE.ordinal() == field) {
                quote.setLastQty(size);
            }
            topOfBook.addQuote(quote);
        }

        this.lookupBus.publish(new MsgEnvelope(Integer.toString(tickerId), topOfBook.getQuote(tickerId)));
    }

    @Override
    public void tickOptionComputation(int i, int i2, double v, double v2, double v3, double v4, double v5, double v6, double v7, double v8) {

    }

    @Override
    public void tickGeneric(int i, int i2, double v) {

    }

    @Override
    public void tickString(int i, int i2, String s) {
        if (i2 == 48) {
            try {
                TimeSales timeSales = new TimeSales(s);
                this.lookupBus.publish(new MsgEnvelope(Prefix.TimeSales.getValue() + Integer.toString(i), timeSales));
            }
            catch (Exception erp) {
                //erp.printStackTrace();
            }
        }
    }

    @Override
    public void tickEFP(int i, int i2, double v, String s, double v2, int i3, String s2, double v3, double v4) {

    }

    @Override
    public void orderStatus(int orderId, String status, int filled, int remaining,
                            double avgFillPrice, int permId, int parentId, double lastFillPrice,
                            int clientId, String whyHeld) {
        logger.debug("OID:" + orderId +
                        " Status:" + status +
                        " FQty:" + filled +
                        " Rem:" + remaining +
                        " avgPr:" + avgFillPrice +
                        " lastPr:" + lastFillPrice
        );
        this.lookupBus.publish(new MsgEnvelope(EventTopic.ORDERSTATUS.getValue()
                , new OrderStatus(orderId, status, filled, remaining,
                avgFillPrice, permId, parentId, lastFillPrice,
                clientId, whyHeld)));
    }

    @Override
    public void openOrder(int i, Contract contract, Order order, OrderState orderState) {
        logger.debug("orderId " + order.m_orderId);
    }

    @Override
    public void openOrderEnd() {

    }

    @Override
    public void updateAccountValue(String s, String s2, String s3, String s4) {

    }

    @Override
    public void updatePortfolio(Contract contract, int i, double v, double v2, double v3, double v4,
                                double v5, String s) {

    }

    @Override
    public void updateAccountTime(String s) {

    }

    @Override
    public void accountDownloadEnd(String s) {

    }

    @Override
    public void nextValidId(int i) {
        OrderIDGenerator.setOrderID(i);
    }

    @Override
    public void contractDetails(int i, ContractDetails contractDetails) {
        contractLookUp.setContractDetailsMap(i, contractDetails);
        contractLookUp.setContractMap(i, contractDetails.m_summary);
        this.lookupBus.publish(new MsgEnvelope(Integer.toString(i), TradeState.READY));
    }

    @Override
    public void bondContractDetails(int i, ContractDetails contractDetails) {

    }

    @Override
    public void contractDetailsEnd(int i) {

    }

    @Override
    public void execDetails(int reqId, Contract contract, Execution execution) {
        if (logger.isInfoEnabled()) {
            logger.info("FILLED\n OrderID=" + execution.m_orderId +
                    "\n Con=" + contract.m_conId +
                    "\n Price=" + execution.m_price +
                    "\n Side=" + execution.m_side +
                    "\n Qty=" + execution.m_shares +
                    "\n CumQty=" + execution.m_cumQty +
                    "\n reqid=" + reqId);
        }
        this.lookupBus.publish(new MsgEnvelope(EventTopic.FILLS.getValue(), new Fill(contract, execution)));
    }

    @Override
    public void execDetailsEnd(int i) {

    }

    @Override
    public void updateMktDepth(int i, int i2, int i3, int i4, double v, int i5) {

    }

    @Override
    public void updateMktDepthL2(int i, int i2, String s, int i3, int i4, double v, int i5) {

    }

    @Override
    public void updateNewsBulletin(int i, int i2, String s, String s2) {

    }

    @Override
    public void managedAccounts(String s) {

    }

    @Override
    public void receiveFA(int i, String s) {

    }

    @Override
    public void historicalData(int i, String s, double v, double v2, double v3, double v4, int i2, int i3,
                               double v5, boolean b) {

    }

    @Override
    public void scannerParameters(String s) {

    }

    @Override
    public void scannerData(int i, int i2, ContractDetails contractDetails, String s, String s2, String s3, String
            s4) {

    }

    @Override
    public void scannerDataEnd(int i) {

    }

    @Override
    public void realtimeBar(int i, long l, double v, double v2, double v3, double v4, long l2, double v5, int i2) {

    }

    @Override
    public void currentTime(long l) {

    }

    @Override
    public void fundamentalData(int i, String s) {

    }

    @Override
    public void deltaNeutralValidation(int i, UnderComp underComp) {

    }

    @Override
    public void tickSnapshotEnd(int i) {

    }

    @Override
    public void marketDataType(int i, int i2) {

    }

    @Override
    public void commissionReport(CommissionReport commissionReport) {

    }

    @Override
    public void position(String s, Contract contract, int i, double v) {

    }

    @Override
    public void positionEnd() {

    }

    @Override
    public void accountSummary(int i, String s, String s2, String s3, String s4) {

    }

    @Override
    public void accountSummaryEnd(int i) {

    }


    @Override
    public void error(Exception e) {
        e.printStackTrace();
    }

    @Override
    public void error(String s) {
        System.out.println(s);
    }

    @Override
    public void error(int id, int errorCode, String errorMsg) {
        logger.info("UID= " + id + " MSG= " + errorMsg + " ErrCode= " + errorCode);
        System.out.println("UID= " + id + " MSG= " + errorMsg + " ErrCode= " + errorCode);
    }

    @Override
    public void connectionClosed() {
        logger.info("Connection Closed");
    }


}
