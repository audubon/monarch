package prc.ib;

/**
 * User: axs
 * Date: 12/22/2014
 * Time: 4:29 PM
 */
public interface Constants {
    // constants - tick types
    static final int BID_SIZE = 0;
    static final int BID = 1;
    static final int ASK = 2;
    static final int ASK_SIZE = 3;
    static final int LAST = 4;
    static final int LAST_SIZE = 5;
    static final int HIGH = 6;
    static final int LOW = 7;
    static final int VOLUME = 8;
    static final int CLOSE = 9;

    static final int MYBID = 3;
    static final int MYASK = 4;
    static final int EMAVG = 5;
    static final int EMAO = 6;

    //tickerId for requested data
    static final int USH6 = 1;
    static final int TYH6 = 2;
    static final int FVH6 = 3;
    static final int TUH6 = 4;
    static final int NKZ5 = 69;
    static final int NKH6 = 68;
    static final int CLF6 = 70;
    static final int BCF6 = 71;
    static final int BCG6 = 72;
    static final int BCH6 = 73;
    static final int CLG6 = 74;
    static final int GOF6 = 75;
    static final int GOG6 = 76;

    static final int NOT_AN_FA_ACCOUNT_ERROR = 321;
    static final int faErrorCodes[] = {503, 504, 505, 522, 1100, NOT_AN_FA_ACCOUNT_ERROR};


}
