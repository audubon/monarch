package prc.ib;

import java.io.Serializable;

/**
 * User: axs
 * Date: 12/29/2014
 * Time: 12:28 PM
 */
public final class OrderStatus implements Serializable{
    private static final long serialVersionUID = 3156680481241L;

    private final int orderId;
    private final String status;
    private final int filled;
    private final int remaining;
    private final double avgFillPrice;
    private final int permId;
    private final int parentId;
    private final double lastFillPrice;
    private final int clientId;
    private final String whyHeld;

    public OrderStatus(int orderId, String status, int filled, int remaining, double avgFillPrice, int permId, int parentId, double lastFillPrice, int clientId, String whyHeld) {
        this.orderId = orderId;
        this.status = status;
        this.filled = filled;
        this.remaining = remaining;
        this.avgFillPrice = avgFillPrice;
        this.permId = permId;
        this.parentId = parentId;
        this.lastFillPrice = lastFillPrice;
        this.clientId = clientId;
        this.whyHeld = whyHeld;
    }


    public int getOrderId() {
        return orderId;
    }

    public String getStatus() {
        return status;
    }

    public int getFilled() {
        return filled;
    }

    public int getRemaining() {
        return remaining;
    }

    public double getAvgFillPrice() {
        return avgFillPrice;
    }

    public int getPermId() {
        return permId;
    }

    public int getParentId() {
        return parentId;
    }

    public double getLastFillPrice() {
        return lastFillPrice;
    }

    public int getClientId() {
        return clientId;
    }

    public String getWhyHeld() {
        return whyHeld;
    }
}
