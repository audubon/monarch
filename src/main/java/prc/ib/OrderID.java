package prc.ib;

import java.io.Serializable;

/**
 * User: axs
 * Date: 12/29/2014
 * Time: 8:56 AM
 */
public class OrderID implements Serializable {
    private static final long serialVersionUID = 30961236581241L;
    private final int orderId;
    private final int orderRequestID;

    public OrderID(int orderId,int orderRequestID) {
        this.orderId = orderId;
        this.orderRequestID = orderRequestID;
    }

    public int getOrderId() {
        return orderId;
    }

    public int getOrderRequestID() {
        return orderRequestID;
    }
}
