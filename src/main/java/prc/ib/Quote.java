package prc.ib;

import java.io.Serializable;

/**
 * User: axs
 * Date: 12/22/2014
 * Time: 3:51 PM
 */
public final class Quote implements Serializable {
    private static final long serialVersionUID = 1L;
    private double bid;
    private double ask;
    private double last;
    private int lastQty;
    private int bidQty;
    private int askQty;
    private String symbol;
    private final int contractID;


    public Quote(Quote quote) {
        bid = quote.getBid();
        ask = quote.getAsk();
        last = quote.getLast();
        lastQty = quote.getLastQty();
        bidQty = quote.getBidQty();
        askQty = quote.getAskQty();
        symbol = quote.getSymbol();
        contractID = quote.getContractID();
    }

    public Quote(int contractID) {
        this.contractID = contractID;
    }

    public void setBid(double bid) {
        this.bid = bid;
    }

    public void setAsk(double ask) {
        this.ask = ask;
    }

    public String getSymbol() {
        return symbol;
    }

    public double getBid() {
        return bid;
    }

    public double getAsk() {
        return ask;
    }

    public double getMid() {
        return (bid + ask) * .5;
    }

    public double getLast() {
        return last;
    }

    public int getLastQty() {
        return lastQty;
    }

    public int getBidQty() {
        return bidQty;
    }

    public int getAskQty() {
        return askQty;
    }

    public int getContractID() {
        return contractID;
    }

    public void setAskQty(int askQty) {
        this.askQty = askQty;
    }

    public void setLast(double last) {
        this.last = last;
    }

    public void setLastQty(int lastQty) {
        this.lastQty = lastQty;
    }

    public void setBidQty(int bidQty) {
        this.bidQty = bidQty;
    }

    public boolean isValid() {
        return getAsk() > getBid();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Quote quote = (Quote) o;

        if (Double.compare(quote.ask, ask) != 0) return false;
        if (askQty != quote.askQty) return false;
        if (Double.compare(quote.bid, bid) != 0) return false;
        if (bidQty != quote.bidQty) return false;
        if (contractID != quote.contractID) return false;
        if (Double.compare(quote.last, last) != 0) return false;
        if (lastQty != quote.lastQty) return false;
        if (!symbol.equals(quote.symbol)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(bid);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(ask);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(last);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + lastQty;
        result = 31 * result + bidQty;
        result = 31 * result + askQty;
        result = 31 * result + symbol.hashCode();
        result = 31 * result + contractID;
        return result;
    }


    @Override
    public String toString() {
        return "Quote{" +
                "bid=" + bid +
                ", ask=" + ask +
                ", last=" + last +
                ", lastQty=" + lastQty +
                ", bidQty=" + bidQty +
                ", askQty=" + askQty +
                ", symbol='" + symbol + '\'' +
                ", contractID=" + contractID +
                '}';
    }
}
