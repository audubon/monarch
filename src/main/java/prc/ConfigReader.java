package prc;

import com.ib.client.Contract;
import org.codehaus.jackson.map.ObjectMapper;
import prc.trading.ContractLookUp;
import prc.trading.conf.ContractConfig;
import prc.trading.conf.MonarchConf;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

/**
 * User: axs
 * Date: 12/23/2014
 * Time: 7:53 AM
 */
public class ConfigReader {
    public static ContractConfig[] readContractConfigFile() {
        ContractConfig[] config = null;
        ObjectMapper mapper = new ObjectMapper();
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(ConfigReader.class.getResourceAsStream("/contracts.json")));
            config = mapper.readValue(br, ContractConfig[].class);
        }
        catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }
        return config;
    }


    public static MonarchConf readMonarchConfFile() {
        MonarchConf config = null;
        ObjectMapper mapper = new ObjectMapper();
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(ConfigReader.class.getResourceAsStream("/monarch.conf")));
            config = mapper.readValue(br, MonarchConf.class);
        }
        catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }
        return config;
    }




}
