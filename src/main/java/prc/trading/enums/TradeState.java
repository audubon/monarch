package prc.trading.enums;

import java.util.EnumSet;

/**
 * User: axs
 * Date: 12/26/2014
 * Time: 9:12 AM
 */

public enum TradeState {
    READY, IDLE, TRADING, SUSPEND, HALTED, COMPLETE, DISABLE, RISKLIMIT, HEDGELIMIT, INVALID, BADQUOTE, ALGOSUSPEND, HEDGING, SHUTDOWN, QUOTES;


    public static final EnumSet<TradeState> OKTOENABLE = EnumSet.of(TradeState.DISABLE, TradeState.COMPLETE);
    public static final EnumSet<TradeState> ISTRADING = EnumSet.of(TradeState.TRADING, TradeState.ALGOSUSPEND, TradeState.HEDGELIMIT);

    public final boolean canEnable() {
        return OKTOENABLE.contains(this);
    }

    public final boolean isTrading() {
        return ISTRADING.contains(this);
    }
}
