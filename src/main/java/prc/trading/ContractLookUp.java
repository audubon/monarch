package prc.trading;

import com.ib.client.ComboLeg;
import com.ib.client.Contract;
import com.ib.client.ContractDetails;
import prc.ib.SecType;
import prc.ib.Side;

import java.util.Map;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;

/**
 * User: axs
 * Date: 12/26/2014
 * Time: 8:44 AM
 */
public final class ContractLookUp {
    private static ContractLookUp INSTANCE = new ContractLookUp();
    private final Map<Integer, Contract> contractMap;
    private final Map<Integer, ContractDetails> contractDetailsMap;

    public static ContractLookUp getInstance() {
        return INSTANCE;
    }

    private ContractLookUp() {
        this.contractMap = new ConcurrentHashMap<>();
        this.contractDetailsMap = new ConcurrentHashMap<>();
    }

    public Map<Integer, Contract> getContractMap() {
        return this.contractMap;
    }

    public Contract getContract(int k) {
        return this.contractMap.get(k);
    }

    public Map<Integer, ContractDetails> getContractDetailsMap() {
        return this.contractDetailsMap;
    }

    public ContractDetails getContractDetails(int k) {
        return this.contractDetailsMap.get(k);
    }

    public void setContractMap(final Map<Integer, Contract> contractMap) {
        this.contractMap.putAll(contractMap);
    }

    public void setContractMap(int i, Contract contract) {
        this.contractMap.put(i, contract);
    }

    public void setContractDetailsMap(int i, ContractDetails contractDetails) {
        this.contractDetailsMap.put(i, contractDetails);
    }

    public String getContractTicker(int k) {
        Contract contract = getContract(k);
        return contract.m_symbol + (contract.m_expiry == null ? "" : contract.m_expiry);
    }

    @SuppressWarnings("unchecked")
    public static Contract createCombo(int conidOne, int conidTwo, String symbol, String exchange) {
        ComboLeg leg1 = new ComboLeg();
        ComboLeg leg2 = new ComboLeg();
        Vector addAllLegs = new Vector();

        leg1.m_conId = conidOne;
        leg1.m_ratio = 1;
        leg1.m_action = Side.BUY.getValue();
        leg1.m_exchange = exchange;
        leg1.m_openClose = 0;
        leg1.m_shortSaleSlot = 0;
        leg1.m_designatedLocation = "";

        leg2.m_conId = conidTwo;
        leg2.m_ratio = 1;
        leg2.m_action = Side.SELL.getValue();
        leg2.m_exchange = exchange;
        leg2.m_openClose = 0;
        leg2.m_shortSaleSlot = 0;
        leg2.m_designatedLocation = "";

        addAllLegs.add(leg1);
        addAllLegs.add(leg2);

        Contract contract = new Contract();
        contract.m_symbol = "USD";
        contract.m_secType = SecType.BAG.getValue();
        contract.m_exchange = exchange;
        contract.m_currency = "USD";
        contract.m_comboLegs = addAllLegs;
        contract.m_symbol = symbol;

        return contract;
    }
}
