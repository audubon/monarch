package prc.trading.strategy;

import akka.actor.DeadLetter;
import akka.actor.UntypedActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;


/**
 * User: axs
 * Date: 12/23/2014
 * Time: 1:06 PM
 */
public class DeadLetterActor extends UntypedActor {
    private final LoggingAdapter log = Logging.getLogger(getContext().system(), "monarch");

    public void onReceive(Object message) {
        if (message instanceof DeadLetter) {
            log.warning(message.toString());
        }
    }
}
