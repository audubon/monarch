package prc.trading.strategy;

import akka.actor.*;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.japi.Creator;
import com.ib.client.Contract;
import com.ib.client.Execution;
import com.ib.client.Order;
import prc.ib.*;
import prc.messaging.akka.notifications.AnnouncementCodes;
import prc.messaging.zeromq.Command;
import prc.messaging.zeromq.CommandType;
import prc.trading.ContractLookUp;
import prc.trading.OrderManager;
import prc.trading.enums.TradeState;
import prc.utils.MathUtils;
import scala.concurrent.duration.Duration;
import scala.concurrent.duration.FiniteDuration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * User: axs
 * Date: 12/23/2014
 * Time: 11:57 AM
 */
public class TradeActor extends UntypedActor {
    private final LoggingAdapter log = Logging.getLogger(getContext().system(), "monarch");
    private final String myname;
    private final String shortname;
    private final ContractLookUp contractLookUp = ContractLookUp.getInstance();
    private final Map<Integer, Quote> quoteMap;
    private TradeState tradeState;
    private final OrderManager oms;
    private final Side side;
    private final Side bracketside;
    private Cancellable cxlrepl;


    public static Props props(Side side) {
        return Props.create(new Creator<TradeActor>() {
            private static final long serialVersionUID = 1L;

            @Override
            public TradeActor create() throws Exception {
                return new TradeActor(side);
            }
        });
    }

    public TradeActor(Side side) {
        this.shortname = getSelf().path().name();
        this.myname = getSelf().toString();
        this.tradeState = TradeState.IDLE;
        this.oms = new OrderManager();
        this.quoteMap = new HashMap<>();
        this.side = side;
        this.bracketside = side.getFlip();

        initialTrade();
    }


    private void initialTrade() {
        getContext().system().scheduler().scheduleOnce(Duration.create(MathUtils.randInt(7, 12), TimeUnit.SECONDS),
                (Runnable) () -> trade()
                , getContext().system().dispatcher());
    }

    private void trade() {
        log.info("trade {} {}", myname, shortname);
        this.tradeState = TradeState.TRADING;
        Quote qt = (Quote) quoteMap.values().toArray()[0];
        double tick = contractLookUp.getContractDetails(qt.getContractID()).m_minTick;
        double mktPrice = this.side == Side.BUY ? qt.getBid() - tick * 3 : qt.getAsk() + tick * 3;
        double coverPrice = this.bracketside == Side.SELL ? mktPrice + tick * 6 : mktPrice - tick * 6;
        Order order = OrderBuilder.createLimitOrder(mktPrice, this.side);
        Order border = OrderBuilder.createLimitOrder(coverPrice, this.bracketside);
        border.m_tif = TimeInForce.GTC.getValue();
        //Order order = OrderBuilder.createOrder(Side.BUY);
        //Order order = OrderBuilder.createOrder();
        Contract contract = contractLookUp.getContract(qt.getContractID());


        OrderRequest orderRequest = new OrderRequest(contract, order, qt.getContractID());
        OrderRequest borderRequest = new OrderRequest(contract, border, qt.getContractID());
        this.oms.add(orderRequest);
        this.oms.add(borderRequest);

        BracketOrderRequest bracketOrderRequest = new BracketOrderRequest(new OrderRequest[]{orderRequest, borderRequest});
        getContext().actorSelection("/user/gatewayActor").tell(bracketOrderRequest, getSelf());


        //used for sending latest quote
        FiniteDuration duration = Duration.create(8, TimeUnit.SECONDS);
        FiniteDuration iniduration = Duration.create(MathUtils.randInt(7, 15), TimeUnit.SECONDS);
        cxlrepl = getContext().system().scheduler().schedule(iniduration, duration, getSelf()
                , AnnouncementCodes.STATUS, getContext().system().dispatcher(), null);
    }


    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof Quote) {
            Quote qt = (Quote) message;
            //log.info("{} {}", qt, myname);
            this.quoteMap.put(qt.getContractID(), qt);
        }
        else if (message instanceof OrderID) {
            OrderID orderID = (OrderID) message;
            this.oms.setorderID2orderReqID(orderID);
            this.oms.getOrder(orderID.getOrderRequestID()).getOrder().m_orderId = orderID.getOrderId();
        }
        else if (message instanceof Fill) {
            Fill fill = (Fill) message;
            if (this.oms.isWorking(fill.getExecution().m_orderId)) {
                this.oms.getPortfolio().addFill(fill);
                Execution execution = fill.getExecution();
                Contract contract = fill.getContract();
                log.info("FILL OrderID={} Con={} Price={} Side={} CumQty={}", execution.m_orderId, contract.m_conId, execution.m_price, execution.m_cumQty);

            }
        }
        else if (message instanceof OrderStatus) {
            OrderStatus orderStatus = (OrderStatus) message;
            if (this.oms.isWorking(orderStatus.getOrderId())) {
                if (orderStatus.getStatus().equals("Filled"))
                    this.oms.removeOrder(orderStatus.getOrderId());
                log.info("OrderStatus OrderID={}  Status={}", orderStatus.getOrderId(), orderStatus.getStatus());
            }
        }
        else if (message instanceof TradeState) {
            if (message == TradeState.READY) {
                this.tradeState = TradeState.READY;
                log.info("ready {}", myname);
            }
        }
        else if (message instanceof Command) {
            Command command = (Command) message;
            if (command.getAction() == CommandType.DISABLELIQUIDITY) {
                log.info("received disable  {}", myname);
                cxlrepl.cancel();

                ActorSelection gateway = getContext().actorSelection("/user/gatewayActor");
                //OrderRequest orderRequest = this.oms.getOrders().values().stream().filter(o -> o.getOrder().m_action.equals(Side.BUY.getValue())).findFirst().get();
                if (this.oms.getOrders().values().size() >1) {
                    for (OrderRequest orderRequest : this.oms.getOrders().values()) {
                        gateway.tell(new Cancel(orderRequest.getOrder().m_orderId), getSelf());
                    }
                }
                sendPoisonPill();
            }
            else if (command.getAction() == CommandType.ENABLE) {
                log.info("received ENABLE  {}", myname);
                if (tradeState != TradeState.TRADING)
                    initialTrade();
            }
            else if (command.getAction() == CommandType.RETREAT || command.getAction() == CommandType.ATTACK) {
                log.info("received {}  {}", command.getAction(), myname);
                if (tradeState == TradeState.TRADING)
                    cancelReplace(command);
            }

        }
        else if (message instanceof AnnouncementCodes) {
            if (message == AnnouncementCodes.STATUS && (tradeState == TradeState.READY || tradeState == TradeState.TRADING)) {
                try {
                    if (this.oms.getOrders().values().size() < 2) {
                        return;
                    }
                    ActorSelection gateway = getContext().actorSelection("/user/gatewayActor");
                    //OrderRequest orderRequest = this.oms.getOrders().values().stream().filter(o -> o.getOrder().m_action.equals(Side.BUY.getValue())).findFirst().get();
                    for (OrderRequest orderRequest : this.oms.getOrders().values()) {
                        double tick = contractLookUp.getContractDetails(orderRequest.getContractID()).m_minTick;
                        int offset = orderRequest.getOrder().m_action.equals(Side.BUY.getValue()) ? -6 : 6;
                        double mktPrice = this.side == Side.BUY ? quoteMap.get(orderRequest.getContractID()).getBid() : quoteMap.get(orderRequest.getContractID()).getAsk();
                        orderRequest.getOrder().m_lmtPrice = mktPrice + tick * offset;
                        gateway.tell(new CancelReplace(orderRequest), getSelf());
                    }
                }
                catch (Exception erp) {
                    erp.printStackTrace();
                }
            }
        }
        else
            unhandled(message);
    }


    private void cancelReplace(Command command) {
        try {
            if (this.oms.getOrders().values().size() < 2) {
                return;
            }
            ActorSelection gateway = getContext().actorSelection("/user/gatewayActor");
            //OrderRequest orderRequest = this.oms.getOrders().values().stream().filter(o -> o.getOrder().m_action.equals(Side.BUY.getValue())).findFirst().get();
            for (OrderRequest orderRequest : this.oms.getOrders().values()) {
                double tick = contractLookUp.getContractDetails(orderRequest.getContractID()).m_minTick;
                int offset = orderRequest.getOrder().m_action.equals(Side.BUY.getValue()) ? -1 : 1;
                offset *= command.getAction() == CommandType.ATTACK ? -1 : 1;
                orderRequest.getOrder().m_lmtPrice += tick * offset;
                gateway.tell(new CancelReplace(orderRequest), getSelf());
            }
        }
        catch (Exception erp) {
            erp.printStackTrace();
        }
    }

    private void sendPoisonPill() {
        getContext().system().scheduler().scheduleOnce(Duration.create(1, TimeUnit.SECONDS),
                getSelf(), PoisonPill.getInstance(), getContext().system().dispatcher(), null);
    }


    @Override
    public void preStart() {
        log.info("prestart: {}", myname);
    }

    @Override
    public void preRestart(Throwable reason, scala.Option<Object> message) {
        log.info("preREstart {}", myname);
        for (ActorRef each : getContext().getChildren()) {
            getContext().unwatch(each);
            getContext().stop(each);
        }
        postStop();
    }

    @Override
    public void postStop() {
        log.info("postStop: {}", myname);
    }
}
