package prc.trading.strategy;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.japi.Creator;
import com.ib.client.Contract;
import prc.ib.SecType;
import prc.ib.Side;
import prc.messaging.akka.EventTopic;
import prc.messaging.akka.LookupBusImpl;
import prc.messaging.zeromq.Command;
import prc.messaging.zeromq.CommandType;
import prc.trading.ContractLookUp;
import prc.utils.MathUtils;
import scala.concurrent.duration.Duration;

import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * User: axs
 * Date: 1/26/2015
 * Time: 10:35 AM
 */
public class TradeCoordinatorActor extends UntypedActor {
    private final LoggingAdapter log = Logging.getLogger(getContext().system(), "monarch");
    private String myname = getSelf().toString();
    private final ContractLookUp contractLookUp = ContractLookUp.getInstance();
    private final LookupBusImpl lookupBus;

    public static Props props(LookupBusImpl lookupBus) {
        return Props.create(new Creator<TradeCoordinatorActor>() {
            private static final long serialVersionUID = 63548941371L;

            @Override
            public TradeCoordinatorActor create() throws Exception {
                return createPairsTradeActor(lookupBus);
            }
        });
    }

    TradeCoordinatorActor(LookupBusImpl lookupBus) {
        this.lookupBus = lookupBus;


    }

    public static TradeCoordinatorActor createPairsTradeActor(LookupBusImpl lookupBus) {
        return new TradeCoordinatorActor(lookupBus);
    }

    private void createTradeActors(Side side) {
        for (Map.Entry<Integer, Contract> entry : contractLookUp.getContractMap().entrySet()) {
            String actorname = entry.getValue().m_symbol + entry.getValue().m_expiry;
            if (entry.getValue().m_secType.equals(SecType.FOP.getValue()) || entry.getValue().m_secType.equals(SecType.OPT.getValue())) {
                actorname += entry.getValue().m_strike + entry.getValue().m_right;
            }
            final ActorRef tradeActor = getContext().actorOf(TradeActor.props(side), actorname);
            lookupBus.subscribe(tradeActor, entry.getKey().toString());
            lookupBus.subscribe(tradeActor, EventTopic.FILLS.getValue());
            lookupBus.subscribe(tradeActor, EventTopic.ORDERSTATUS.getValue());
        }
    }

    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof Command) {
            Command command = (Command) message;
            if (command.getAction() == CommandType.DISABLEALL) {
                log.debug("disable all {}", myname);
                handleDisableAll();
            }
            else if (command.getAction() == CommandType.ENABLE) {
                log.debug("disable all {}", myname);
                createTradeActors(Side.valueOf(command.getArgument()));
            }
            else if (command.getAction().isLiquidCommand()) {
                log.debug("{} {}",command.getAction(), myname);
                handleLiquidityCommand(command);
            }
        }
        else
            unhandled(message);
    }

    private void handleLiquidityCommand(Command command) {
        for (ActorRef child : getContext().getChildren()) {
            child.tell(command, getSelf());
        }
    }

    private void handleDisableAll() {
        for (ActorRef child : getContext().getChildren()) {
            child.tell(Command.create(child.toString(), CommandType.DISABLELIQUIDITY), getSelf());
            this.lookupBus.unsubscribe(child);
        }
    }

    @Override
    public void preStart() {
        log.info("prestart: {}", myname);
    }

    @Override
    public void preRestart(Throwable reason, scala.Option<Object> message) {
        log.info("preREstart {}", myname);
        for (ActorRef each : getContext().getChildren()) {
            getContext().unwatch(each);
            getContext().stop(each);
        }
        postStop();
    }

    @Override
    public void postStop() {
        log.info("postStop: {}", myname);
    }
}
