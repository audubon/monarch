package prc.trading.strategy;

import akka.actor.ActorRef;
import akka.actor.ActorSelection;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.japi.Creator;

import com.espertech.esper.client.*;
import com.ib.client.Contract;
import prc.ib.*;
import prc.messaging.zeromq.Message;
import prc.messaging.zeromq.Topics;
import prc.trading.ContractLookUp;


import java.util.HashMap;
import java.util.Map;

/**
 * User: axs
 * Date: 12/30/2014
 * Time: 3:07 PM
 */
public class SignalActor extends UntypedActor {
    private final LoggingAdapter log = Logging.getLogger(getContext().system(), "monarch");
    private final String myname;
    private final String shortname;
    private final ContractLookUp contractLookUp = ContractLookUp.getInstance();
    private final Map<Integer, Quote> quoteMap;
    private EPServiceProvider epService;

    public static Props props() {
        return Props.create(new Creator<SignalActor>() {
            private static final long serialVersionUID = 1L;

            @Override
            public SignalActor create() throws Exception {
                return new SignalActor();
            }
        });
    }

    public SignalActor() {
        this.shortname = getSelf().path().name();
        this.myname = getSelf().toString();
        this.quoteMap = new HashMap<>();
    }


    private void esperInit() {
        //getContext().actorSelection("/user/gatewayActor").tell(new CancelReplace(orderRequest), getSelf());
        ActorSelection publisher = getContext().actorSelection("/user/publisherActor");
        epService = EPServiceProviderManager.getDefaultProvider();
        String expression = "select istream contractID,average,variance,stddev from prc.ib.Quote.std:groupwin(contractID).win:time(60 sec).stat:uni(mid) output snapshot  every 10 seconds";
        //String expression = "select istream contractID,avg(mid) from prc.ib.Quote.win:time(30 sec) group by contractID output last every 10 seconds";
        EPStatement statement = epService.getEPAdministrator().createEPL(expression);

        UpdateListener listener = new UpdateListener() {
            public void update(EventBean[] newEvents, EventBean[] oldEvents) {
                //EventBean event = newEvents[0];
                for (EventBean event : newEvents) {
                    //System.out.println(event.get("contractID") + " avg=" + event.get("average") + " var=" + event.get("variance"));

                    String ticker = contractLookUp.getContractTicker((Integer) event.get("contractID"));
                    double mid = quoteMap.get((Integer) event.get("contractID")).getMid();
                    Map<String, Object> k = new HashMap<>();
                    k.put("Symbol", ticker);
                    k.put("Average", event.get("average"));
                    k.put("Variance", event.get("variance"));
                    k.put("StdDev", event.get("stddev"));
                    k.put("MidPrice", mid);
                    if (mid > (Double) event.get("average"))
                        k.put("Bias", "LONG");
                    else
                        k.put("Bias", "SHORT");
                    publisher.tell(new Message(Topics.SIGNAL.getValue(), k), getSelf());
                }
            }
        };
        statement.addListener(listener);
    }


    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof Quote) {
            Quote qt = (Quote) message;
            //log.info("{} {}", qt.getMid(), myname);
            this.quoteMap.put(qt.getContractID(), qt);
            epService.getEPRuntime().sendEvent(new Quote(qt));
        }
        else
            unhandled(message);
    }


    @Override
    public void preStart() {
        log.info("prestart: {}", myname);

        new Thread(
                this::esperInit
        ).start();
    }

    @Override
    public void preRestart(Throwable reason, scala.Option<Object> message) {
        log.info("preREstart {}", myname);
        for (ActorRef each : getContext().getChildren()) {
            getContext().unwatch(each);
            getContext().stop(each);
        }
        postStop();
    }

    @Override
    public void postStop() {
        log.info("postStop: {}", myname);
    }


}
