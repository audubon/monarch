package prc.trading.strategy;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.japi.Creator;
import prc.ConfigReader;
import prc.ib.*;
import prc.messaging.akka.LookupBusImpl;
import prc.trading.ContractLookUp;
import prc.trading.conf.MonarchConf;
import prc.utils.MathUtils;

/**
 * User: axs
 * Date: 12/23/2014
 * Time: 10:44 AM
 */
public class GatewayActor extends UntypedActor {
    private final LoggingAdapter log = Logging.getLogger(getContext().system(), "monarch");
    private String myname = getSelf().toString();
    private final Connection connection;
    private final ContractLookUp contractLookUp = ContractLookUp.getInstance();
    private final LookupBusImpl lookupBus;

    public static Props props(final LookupBusImpl lookupBus) {
        return Props.create(new Creator<GatewayActor>() {
            private static final long serialVersionUID = 1L;

            @Override
            public GatewayActor create() throws Exception {
                return new GatewayActor(lookupBus);
            }
        });
    }


    public GatewayActor(final LookupBusImpl lookupBus) {
        this.connection = new Connection(lookupBus);
        this.lookupBus = lookupBus;
        MonarchConf etc = ConfigReader.readMonarchConfFile();
        this.connection.conn(MathUtils.randInt(50, 500), etc.getIBEndpoint());
        this.connection.getMktDataOnly(contractLookUp.getContractMap());
        this.connection.reqConDetails(contractLookUp.getContractMap());
    }

    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof OrderRequest) {
            OrderRequest orderRequest = (OrderRequest) message;
            int oid = OrderIDGenerator.getAndIncrement();
            orderRequest.getOrder().m_orderId = oid;
            getSender().tell(new OrderID(oid, orderRequest.getOrderReqID()), getSelf());
            this.connection.placeOrder(orderRequest);
            log.info("got orderrequest {} {}", orderRequest.getOrder().m_orderId, myname);
        }
        else if (message instanceof CancelReplace) {
            CancelReplace cancelReplace = (CancelReplace) message;
            this.connection.placeOrder(cancelReplace.getOrderRequest());
            log.info("got CancelReplace {} {}", cancelReplace.getOrderRequest().getOrder().m_orderId, myname);
        }
        else if (message instanceof Cancel) {
            Cancel cancel = (Cancel) message;
            this.connection.cancelOrder(cancel.getOrderid());
            log.info("got Cancel {} {}", cancel.getOrderid(), myname);
        }
        else if (message instanceof BracketOrderRequest) {
            BracketOrderRequest bracketOrderRequest = (BracketOrderRequest) message;
            int oid = OrderIDGenerator.getAndIncrement();
            int i = 0;
            for (OrderRequest orderRequest : bracketOrderRequest.getOrderRequestList()) {
                orderRequest.getOrder().m_parentId = oid;
                orderRequest.getOrder().m_orderId = i++ == 0 ? oid : OrderIDGenerator.getAndIncrement();
                getSender().tell(new OrderID(orderRequest.getOrder().m_orderId, orderRequest.getOrderReqID()), getSelf());
                this.connection.placeOrder(orderRequest);
                log.info("got BracketOrderRequest {} {} {}", orderRequest.getOrder().m_orderId, orderRequest.getOrder().m_lmtPrice, orderRequest.getContract().m_symbol);
            }
        }
        else
            unhandled(message);
    }


    @Override
    public void preStart() {
        log.info("prestart: {}", myname);
    }

    @Override
    public void preRestart(Throwable reason, scala.Option<Object> message) {
        log.info("preREstart {}", myname);
        for (ActorRef each : getContext().getChildren()) {
            getContext().unwatch(each);
            getContext().stop(each);
        }
        postStop();
    }

    @Override
    public void postStop() {
        log.info("postStop: {}", myname);
    }
}
