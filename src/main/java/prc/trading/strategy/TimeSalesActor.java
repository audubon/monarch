package prc.trading.strategy;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import prc.ib.TimeSales;
import prc.trading.ContractLookUp;
import akka.japi.Creator;

/**
 * User: axs
 * Date: 1/2/2015
 * Time: 11:30 AM
 */
public class TimeSalesActor extends UntypedActor {
    private final LoggingAdapter log = Logging.getLogger(getContext().system(), "monarch");
    private final String myname;
    private final String shortname;
    private final ContractLookUp contractLookUp = ContractLookUp.getInstance();

    public static Props props() {
        return Props.create(new Creator<TimeSalesActor>() {
            private static final long serialVersionUID = 1L;

            @Override
            public TimeSalesActor create() throws Exception {
                return new TimeSalesActor();
            }
        });
    }

    public TimeSalesActor() {
        this.shortname = getSelf().path().name();
        this.myname = getSelf().toString();

    }


    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof TimeSales) {
            TimeSales timeSales = (TimeSales) message;
            log.info("{} {}",shortname,timeSales);
        }
        else
            unhandled(message);
    }


    @Override
    public void preStart() {
        log.info("prestart: {}", myname);
    }

    @Override
    public void preRestart(Throwable reason, scala.Option<Object> message) {
        log.info("preREstart {}", myname);
        for (ActorRef each : getContext().getChildren()) {
            getContext().unwatch(each);
            getContext().stop(each);
        }
        postStop();
    }

    @Override
    public void postStop() {
        log.info("postStop: {}", myname);
    }
}

