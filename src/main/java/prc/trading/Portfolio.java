package prc.trading;

import prc.ib.Fill;
import prc.ib.Side;

import java.util.concurrent.ConcurrentHashMap;

/**
 * User: axs
 * Date: 12/24/2014
 * Time: 7:01 AM
 */
public final class Portfolio {
    private final ConcurrentHashMap<Integer, Position> positions; //ibconid -> position

    public Portfolio() {
        this.positions = new ConcurrentHashMap<>();
    }

    public Position getPosition(int conid) {
        if (this.positions.containsKey(conid))
            return this.positions.get(conid);
        return new Position(conid);
    }

    public ConcurrentHashMap<Integer, Position> getPositions() {
        return this.positions;
    }

    public boolean isPositionsEmpty() {
        return this.positions.isEmpty();
    }

    public void addFill(Fill fill) {
        this.positions.putIfAbsent(fill.getContract().m_conId, new Position(fill.getContract().m_conId));
        if (fill.getExecution().m_side.equals(Side.BOT.getValue()))
            this.getPosition(fill.getContract().m_conId).addQuantity(fill.getExecution().m_shares);
        else
            this.getPosition(fill.getContract().m_conId).decrQuantity(fill.getExecution().m_shares);
    }

    @Override
    public String toString() {
        return "Portfolio{" +
                "positions=" + positions +
                '}';
    }
}
