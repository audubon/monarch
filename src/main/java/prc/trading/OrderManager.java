package prc.trading;

import com.ib.client.Order;
import prc.ib.OrderID;
import prc.ib.OrderRequest;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * User: axs
 * Date: 12/24/2014
 * Time: 7:00 AM
 */
public final class OrderManager {
    private final ConcurrentHashMap<Integer, OrderRequest> orders;
    private final ConcurrentHashMap<Integer, Integer> orderID2orderReqID;
    private final Portfolio portfolio;

    public OrderManager() {
        this.orders = new ConcurrentHashMap<>();
        this.orderID2orderReqID = new ConcurrentHashMap<>();
        this.portfolio = new Portfolio();
    }


    public Map<Integer, OrderRequest> getOrders() {
        return orders;
    }


    public OrderRequest getOrder(Integer myid) {
        if (this.orders.containsKey(myid)) {
            return this.orders.get(myid);
        }
        return null;
    }

    public Portfolio getPortfolio() {
        return this.portfolio;
    }

    public boolean isWorking(Integer morderid) {
        return this.orders.values().stream().anyMatch(p -> p.getOrder().m_orderId == morderid);
        //return this.orders.containsKey(myid);
    }

    public boolean isWorking() {
        return !this.orders.isEmpty();
    }

    public void add(OrderRequest order) {
        this.orders.put(order.getOrderReqID(), order);
    }


    public void removeOrder(Integer oid) {
        int myid = getorderID2orderReqID(oid);
        if (this.orders.containsKey(myid)) {
            this.orders.remove(myid);
        }
    }

    public Position getPosition(int conid) {
        return this.portfolio.getPosition(conid);
    }

    public int getorderID2orderReqID(int oid) {
        return this.orderID2orderReqID.getOrDefault(oid, 0);
    }

    public void setorderID2orderReqID(final OrderID orderID) {
        this.orderID2orderReqID.put(orderID.getOrderId(), orderID.getOrderRequestID());
    }
}
