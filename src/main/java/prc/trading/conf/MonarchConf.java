package prc.trading.conf;

/**
 * User: axs
 * Date: 12/29/2014
 * Time: 4:39 PM
 */
public class MonarchConf {
    public String ZMQReqReplyEndpoint;
    public String ZMQPubSubEndpoint;
    public String IBEndpoint;


    public String getZMQReqReplyEndpoint() {
        return ZMQReqReplyEndpoint;
    }

    public String getZMQPubSubEndpoint() {
        return ZMQPubSubEndpoint;
    }


    public String getIBEndpoint() {
        return IBEndpoint;
    }

    @Override
    public String toString() {
        return "MonarchConf{" +
                "ZMQReqReplyEndpoint='" + ZMQReqReplyEndpoint + '\'' +
                ", ZMQPubSubEndpoint='" + ZMQPubSubEndpoint + '\'' +
                ", IBEndpoint='" + IBEndpoint + '\'' +
                '}';
    }
}
