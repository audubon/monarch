package prc.trading.conf;

import com.ib.client.ComboLeg;
import com.ib.client.Contract;

import java.util.List;
import java.util.Vector;

/**
 * User: axs
 * Date: 1/2/2015
 * Time: 7:57 AM
 */
public class ContractConfig {
    public int m_conId;
    public java.lang.String m_symbol;
    public java.lang.String m_secType;
    public java.lang.String m_expiry;
    public double m_strike;
    public java.lang.String m_right;
    public java.lang.String m_multiplier;
    public java.lang.String m_exchange;
    public java.lang.String m_currency;
    public java.lang.String m_localSymbol;
    public java.lang.String m_primaryExch;
    public boolean m_includeExpired;
    public java.lang.String m_secIdType;
    public java.lang.String m_secId;
    public java.lang.String m_comboLegsDescrip;

    public com.ib.client.UnderComp m_underComp;

    // public java.util.Vector m_comboLegs;

    public List<ComboLeg> m_comboLegs;

    @SuppressWarnings("unchecked")
    public Contract toContract() {
        Contract contract = new Contract();

        contract.m_conId = this.m_conId;
        contract.m_symbol = this.m_symbol;
        contract.m_secType = this.m_secType;
        contract.m_expiry = this.m_expiry;
        contract.m_strike = this.m_strike;
        contract.m_right = this.m_right;
        contract.m_multiplier = this.m_multiplier;
        contract.m_exchange = this.m_exchange;
        contract.m_currency = this.m_currency;
        contract.m_localSymbol = this.m_localSymbol;
        contract.m_primaryExch = this.m_primaryExch;
        contract.m_includeExpired = this.m_includeExpired;
        contract.m_secIdType = this.m_secIdType;
        contract.m_secId = this.m_secId;
        contract.m_comboLegsDescrip = this.m_comboLegsDescrip;
        contract.m_underComp = this.m_underComp;

        if (m_comboLegs != null) {
            Vector newm_comboLegs = new Vector();
            for (ComboLeg comboLeg : m_comboLegs) {
                newm_comboLegs.add(comboLeg);
            }
            contract.m_comboLegs = newm_comboLegs;
        }

        return contract;
    }
}
