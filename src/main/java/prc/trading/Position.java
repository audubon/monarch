package prc.trading;

/**
 * User: axs
 * Date: 12/24/2014
 * Time: 7:01 AM
 */
public final class Position {
    private int conid;
    private int quantity;

    public Position() {
        this.quantity = 0;
    }

    public Position(int conid) {
        this.conid = conid;
        this.quantity = 0;
    }

    public Position(int conid, int qty) {
        this.conid = conid;
        this.quantity = qty;
    }

    public double getQuantity() {
        return this.quantity;
    }


    public int getConId() {
        return this.conid;
    }

    public void addQuantity(int qty) {
        this.quantity += qty;
    }

    public void decrQuantity(int qty) {
        this.quantity -= qty;
    }


    @Override
    public String toString() {
        return "Position{" +
                "conid='" + conid + '\'' +
                ", quantity=" + quantity +
                '}';
    }
}
